﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WarehouseBlazor.MainMigration
{
    public partial class MigrateDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CargosConditionsEnum",
                columns: table => new
                {
                    ConditionOfCargo = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CargosConditions", x => x.ConditionOfCargo);
                });

            migrationBuilder.CreateTable(
                name: "Client",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    UpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Client", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DockConditionsEnum",
                columns: table => new
                {
                    DockCondition = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DockConditions", x => x.DockCondition);
                });

            migrationBuilder.CreateTable(
                name: "Drivers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    UpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Drivers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Trailers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UniqNumnber = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: false),
                    UpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trailers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Warehouses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Address = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    WarehouseTerritoryLenght = table.Column<double>(type: "float", nullable: true),
                    WarehouseTerritoryWidht = table.Column<double>(type: "float", nullable: true),
                    UpperLeftWarhouseCornerX = table.Column<int>(type: "int", nullable: true),
                    UpperLeftWarhouseCornerY = table.Column<int>(type: "int", nullable: true),
                    LowerRightWarehouseCornerX = table.Column<int>(type: "int", nullable: true),
                    LowerRightWarehouseCornerY = table.Column<int>(type: "int", nullable: true),
                    UpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Warehouses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Cargos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Discription = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    Condition = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    UpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cargos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cargos_CargosConditionsEnum",
                        column: x => x.Condition,
                        principalTable: "CargosConditionsEnum",
                        principalColumn: "ConditionOfCargo",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    ClientId = table.Column<int>(type: "int", nullable: false),
                    UpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_Client",
                        column: x => x.ClientId,
                        principalTable: "Client",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Docks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DockNumber = table.Column<int>(type: "int", nullable: false),
                    Condition = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    WarehouseId = table.Column<int>(type: "int", nullable: false),
                    DockLocationCoordinateX = table.Column<double>(type: "float", nullable: true),
                    DockLocationCoordinateY = table.Column<double>(type: "float", nullable: true),
                    DockLenght = table.Column<double>(type: "float", nullable: true),
                    TrailerId = table.Column<int>(type: "int", nullable: true),
                    UpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Docks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Docks_DockConditionsEnum1",
                        column: x => x.Condition,
                        principalTable: "DockConditionsEnum",
                        principalColumn: "DockCondition",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Docks_Docks1",
                        column: x => x.TrailerId,
                        principalTable: "Trailers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Docks_Warehouses",
                        column: x => x.WarehouseId,
                        principalTable: "Warehouses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Sections",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false),
                    WarehouseId = table.Column<int>(type: "int", nullable: false),
                    LeftUpperSectionCornerX = table.Column<int>(type: "int", nullable: true),
                    LeftUpperSectionCornerY = table.Column<int>(type: "int", nullable: true),
                    LeftLowerSectionCornerX = table.Column<int>(type: "int", nullable: true),
                    LeftLowerSectionCornerY = table.Column<int>(type: "int", nullable: true),
                    UpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sections", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sections_Warehouses",
                        column: x => x.WarehouseId,
                        principalTable: "Warehouses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Pallets",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CargoId = table.Column<int>(type: "int", nullable: false),
                    Length = table.Column<double>(type: "float", nullable: true),
                    Width = table.Column<double>(type: "float", nullable: true),
                    Height = table.Column<double>(type: "float", nullable: true),
                    UpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pallets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Pallets_Cargos1",
                        column: x => x.CargoId,
                        principalTable: "Cargos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DeconsolidationServices",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    UpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeconsolidationServices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DeconsolidationServices_Orders",
                        column: x => x.Id,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DeliveryServices",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    DriverId = table.Column<int>(type: "int", nullable: true),
                    ReciperAdress = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeliveryServices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DeliveryServices_Drivers1",
                        column: x => x.DriverId,
                        principalTable: "Drivers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DeliveryServices_Orders",
                        column: x => x.Id,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StoregeServices",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    DateOfStorage = table.Column<DateTime>(type: "date", nullable: false),
                    UpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StoregeServices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StoregeServices_Orders",
                        column: x => x.Id,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Cells",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Row = table.Column<int>(type: "int", nullable: false),
                    CellsColumn = table.Column<int>(type: "int", nullable: false),
                    Stack = table.Column<int>(type: "int", nullable: false),
                    SectionId = table.Column<int>(type: "int", nullable: false),
                    PalletId = table.Column<int>(type: "int", nullable: true),
                    UpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cells", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cells_Pallets",
                        column: x => x.PalletId,
                        principalTable: "Pallets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Cells_Sections",
                        column: x => x.SectionId,
                        principalTable: "Sections",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PalletsInTrailer",
                columns: table => new
                {
                    TrailerId = table.Column<int>(type: "int", nullable: false),
                    PalletId = table.Column<int>(type: "int", nullable: false),
                    UpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PalletsInTrailer", x => new { x.TrailerId, x.PalletId });
                    table.ForeignKey(
                        name: "FK_PalletsInTrailer_Pallets",
                        column: x => x.PalletId,
                        principalTable: "Pallets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PalletsInTrailer_Trailers",
                        column: x => x.TrailerId,
                        principalTable: "Trailers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DeconsolidatedCargos",
                columns: table => new
                {
                    ServiseId = table.Column<int>(type: "int", nullable: false),
                    CargoId = table.Column<int>(type: "int", nullable: false),
                    Destination = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: true),
                    UpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeconsolidatedCargos", x => new { x.ServiseId, x.CargoId });
                    table.ForeignKey(
                        name: "FK_DeconsolidatedCargos_Cargos",
                        column: x => x.CargoId,
                        principalTable: "Cargos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DeconsolidatedCargos_DeconsolidationServices",
                        column: x => x.ServiseId,
                        principalTable: "DeconsolidationServices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CargosToBeDelivered",
                columns: table => new
                {
                    DeliveryServiceId = table.Column<int>(type: "int", nullable: false),
                    CargoId = table.Column<int>(type: "int", nullable: false),
                    Destination = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: true),
                    UpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CargosToBeDelivered", x => new { x.DeliveryServiceId, x.CargoId });
                    table.ForeignKey(
                        name: "FK_CargosToBeDelivered_Cargos",
                        column: x => x.CargoId,
                        principalTable: "Cargos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CargosToBeDelivered_DeliveryServices",
                        column: x => x.DeliveryServiceId,
                        principalTable: "DeliveryServices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StoregeCargos",
                columns: table => new
                {
                    StrogeServiceId = table.Column<int>(type: "int", nullable: false),
                    CargoId = table.Column<int>(type: "int", nullable: false),
                    UpdateDateTime = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StoregeCargos", x => new { x.StrogeServiceId, x.CargoId });
                    table.ForeignKey(
                        name: "FK_StoregeCargos_Cargos",
                        column: x => x.CargoId,
                        principalTable: "Cargos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StoregeCargos_StoregeServices",
                        column: x => x.StrogeServiceId,
                        principalTable: "StoregeServices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cargos_Condition",
                table: "Cargos",
                column: "Condition");

            migrationBuilder.CreateIndex(
                name: "IX_CargosToBeDelivered",
                table: "CargosToBeDelivered",
                column: "CargoId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cells",
                table: "Cells",
                columns: new[] { "Row", "CellsColumn", "Stack", "SectionId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cells_PalletId",
                table: "Cells",
                column: "PalletId");

            migrationBuilder.CreateIndex(
                name: "IX_Cells_SectionId",
                table: "Cells",
                column: "SectionId");

            migrationBuilder.CreateIndex(
                name: "IX_DeconsolidatedCargos_1",
                table: "DeconsolidatedCargos",
                column: "CargoId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DeliveryServices_DriverId",
                table: "DeliveryServices",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_Docks_Condition",
                table: "Docks",
                column: "Condition");

            migrationBuilder.CreateIndex(
                name: "IX_Docks_TrailerId",
                table: "Docks",
                column: "TrailerId");

            migrationBuilder.CreateIndex(
                name: "IX_Docks_WarehouseId",
                table: "Docks",
                column: "WarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_ClientId",
                table: "Orders",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Pallets_CargoId",
                table: "Pallets",
                column: "CargoId");

            migrationBuilder.CreateIndex(
                name: "IX_PalletsInTrailer",
                table: "PalletsInTrailer",
                column: "PalletId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Sections_WarehouseId",
                table: "Sections",
                column: "WarehouseId");

            migrationBuilder.CreateIndex(
                name: "IX_StoregeCargos",
                table: "StoregeCargos",
                column: "CargoId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CargosToBeDelivered");

            migrationBuilder.DropTable(
                name: "Cells");

            migrationBuilder.DropTable(
                name: "DeconsolidatedCargos");

            migrationBuilder.DropTable(
                name: "Docks");

            migrationBuilder.DropTable(
                name: "PalletsInTrailer");

            migrationBuilder.DropTable(
                name: "StoregeCargos");

            migrationBuilder.DropTable(
                name: "DeliveryServices");

            migrationBuilder.DropTable(
                name: "Sections");

            migrationBuilder.DropTable(
                name: "DeconsolidationServices");

            migrationBuilder.DropTable(
                name: "DockConditionsEnum");

            migrationBuilder.DropTable(
                name: "Pallets");

            migrationBuilder.DropTable(
                name: "Trailers");

            migrationBuilder.DropTable(
                name: "StoregeServices");

            migrationBuilder.DropTable(
                name: "Drivers");

            migrationBuilder.DropTable(
                name: "Warehouses");

            migrationBuilder.DropTable(
                name: "Cargos");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "CargosConditionsEnum");

            migrationBuilder.DropTable(
                name: "Client");
        }
    }
}
