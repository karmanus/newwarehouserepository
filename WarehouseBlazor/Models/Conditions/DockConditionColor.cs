﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseBlazor.Models.Conditions
{
    public class DockConditionColor
    {

        public string GetColorCondition(string condition)
        {
            if (condition == "BusyWithABrokenTrailer")
            {
                return "red";
            }
            if (condition == "BusyWithALoadedTrailer")
            {
                return "yellow";
            }
            if (condition == "BusyWithAnEmptyTrailer")
            {
                return "blue";
            }
            if (condition == "Free")
            {
                return "green";
            }
            return "white";


        }

    }
}
