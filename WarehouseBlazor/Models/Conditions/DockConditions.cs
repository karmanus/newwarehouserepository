﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseBlazor.Models.Conditions
{
    public enum DockConditions
    {
        BusyWithABrokenTrailer,
        BusyWithALoadedTrailer,
        BusyWithAnEmptyTrailer,
        Free
    }
}
