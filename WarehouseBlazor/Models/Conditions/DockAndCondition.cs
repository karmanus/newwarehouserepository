﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseBlazor.Models.Conditions
{
    public class DockAndCondition
    {
        public DockAndCondition(int dockId, string dockCondition, int trailerId )
        {
            DockId = dockId;
            DockCondition = dockCondition;
            TrailerId = trailerId;
        }

        public int DockId { get; private set; }

        public string DockCondition { get; set; }

        public int TrailerId { get; set; }
    }
}
