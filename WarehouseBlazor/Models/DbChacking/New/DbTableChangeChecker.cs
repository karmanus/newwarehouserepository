﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Interfaces.DbChangesInterfaces;

namespace WarehouseBlazor.Models.DbChacking.New
{
    public class DbTableChangeChecker<T> where T : IBackEndRepositoryTable
    {        
        public bool CheckIfTheTableHasChanged()
        {
            var currentTableCondition = GetTableCondition();

            if (currentTableCondition.LastTableCount != lastTableCondition.LastTableCount || currentTableCondition.LastTableUpdate != lastTableCondition.LastTableUpdate)
            {
                lastTableCondition = currentTableCondition;
                return true;
            }
            return false;
        }      

        public string TableName { get; }

        public DbTableChangeChecker(IQueryable<T> query)
        {
            this.query = query;
            lastTableCondition = GetTableCondition();
        }

       

        private TableCondition GetTableCondition()
        {
           return new TableCondition { LastTableCount = query.Count(), LastTableUpdate = query.Max(x => x.UpdateDateTime) };
        }
                        

        private TableCondition lastTableCondition;

        private IQueryable<T> query;




    }
}
