﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlazorInputFile;

namespace WarehouseBlazor.Models.Services
{
   public interface IFileUpload
    {
        Task Upload(IFileListEntry file);
    }
}
