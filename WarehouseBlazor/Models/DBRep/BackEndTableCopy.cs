﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Models.DbChacking.New;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Models.DBRep
{

    
    public class BackEndTableCopy<T> where T:  IBackEndRepositoryTable
    {                             

        public BackEndTableCopy(IQueryable<T> query)
        {                                                
            this.query = query;

            entries = query.ToList();

            changeChecker = new DbTableChangeChecker<T>(query);

           
        }

        public List<T> GetEntries()
        {
            return entries.Select(en => (T)en.Clone()).ToList();            
        }

        public void GetChanges()
        {
            if (changeChecker.CheckIfTheTableHasChanged())
            {                                
                entries = query.ToList();
                if (TableHasBeenChanged != null)
                {
                    TableHasBeenChanged();
                }
            }
        }
        
        private void AddRange(List<T> list)
        {
            list.ForEach(en => entries.Add(en));
        }

        public event Action TableHasBeenChanged;

        private readonly IQueryable<T> query;

        private  List<T> entries;

        private DbTableChangeChecker<T> changeChecker;

       

    }
}
