﻿using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Models.DBRep
{
    public  class DbBackEndRepository
    {
        
        private WarehouseBaseContext context;      
        public BackEndTableCopy<Warehouse> Warehouses { get; private set; }
        public BackEndTableCopy<Trailer> Trailers { get; private set; }
        public BackEndTableCopy<StoregeService> StorageServices { get; private set; }
        public BackEndTableCopy<Section> Sections { get; private set; }
        public BackEndTableCopy<Order> Orders { get; private set; }
        public BackEndTableCopy<Driver> Drivers { get; private set; }
        public BackEndTableCopy<Dock> Docks { get; private set; }     
        public BackEndTableCopy<CustomerRepresentative> CustomerRepresentatives { get; private set; }
        public BackEndTableCopy<Cargo> Cargos { get; private set; }
        public BackEndTableCopy<Client> Clients { get; private set; }
        public BackEndTableCopy<Cell> Cells { get; private set; }
        

        public DbBackEndRepository()
        {
            this.context = new WarehouseBaseContext();

            Warehouses = new BackEndTableCopy<Warehouse>( context.Warehouses);

            Trailers = new BackEndTableCopy<Trailer>(context.Trailers);

            StorageServices = new BackEndTableCopy<StoregeService>(context.StoregeServices);

            Sections = new BackEndTableCopy<Section>(context.Sections);

            Orders = new BackEndTableCopy<Order>(context.Orders);

            Drivers = new BackEndTableCopy<Driver>(context.Drivers);

            Docks = new BackEndTableCopy<Dock>(context.Docks);

            CustomerRepresentatives = new BackEndTableCopy<CustomerRepresentative>(context.СustomerRepresentatives);

            Cargos = new BackEndTableCopy<Cargo>(context.Cargos);

            Clients = new BackEndTableCopy<Client>(context.Clients);

            Cells = new BackEndTableCopy<Cell>(context.Cells);
        }

        private void CheckChanges()
        {                     

            do
            {                
                Warehouses.GetChanges();
                Trailers.GetChanges();
                StorageServices.GetChanges();
                Sections.GetChanges();
                Orders.GetChanges();
                Drivers.GetChanges();
                Docks.GetChanges();
                CustomerRepresentatives.GetChanges();
                Cargos.GetChanges();
                Clients.GetChanges();
                Thread.Sleep(1000);
                
            } while (true);
        }

        public async Task OnCheckingAsync()
        {
            Task task = new Task(CheckChanges);
            task.Start();
            await task;
        }       
    }
}
