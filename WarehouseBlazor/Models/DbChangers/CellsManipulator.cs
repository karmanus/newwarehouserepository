﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Models.DbChangers
{
    public class CellsManipulator : ICellsManipulator
    {
        WarehouseBaseContext context;

        public CellsManipulator(WarehouseBaseContext context)
        {
            this.context = context;
        }

        public CellsManipulator()
        {
            context = new WarehouseBaseContext();
        }

        public void AddPalletInCell(int palletId, int cellId)
        {
            throw new NotImplementedException();
        }

        public void AddPalletInTrailer(int palletId, int trailerId)
        {
            throw new NotImplementedException();
        }
    }
}
