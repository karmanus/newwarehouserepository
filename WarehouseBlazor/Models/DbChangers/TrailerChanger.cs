﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Models.DbChangers
{
    public class TrailerChanger : ITrailerChanger
    {

        WarehouseBaseContext context;

        public TrailerChanger()
        {
            context = new WarehouseBaseContext();
        }

        public void AddTrailer(Trailer trailer)
        {
            context.Trailers.Add(trailer);
            context.SaveChanges();
        }
    }
}
