﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Models.DbChangers
{
    public class ClientChanger : IClientChanger
    {
        WarehouseBaseContext context;

        public ClientChanger()
        {
            context = new WarehouseBaseContext();
        }

        public ClientChanger(WarehouseBaseContext context)
        {
            this.context = context;
        }

        public void AddClient(Client client)
        {
            context.Clients.Add(client);
            context.SaveChanges();
        }
    }
}
