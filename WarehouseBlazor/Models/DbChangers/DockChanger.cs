﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Models.DbChangers
{
    public class DockChanger : IDockChanger
    {

        private readonly WarehouseBaseContext context;

        public DockChanger()
        {
            context = new WarehouseBaseContext();
        }

        public DockChanger(WarehouseBaseContext context)
        {
            this.context = context;
        }

        public void AddTrailerOnDock(int dockId, int trailerId)
        {
            if (trailerId > 0)
            {
                context.Docks.Single(dock => dock.Id == dockId).TrailerId = trailerId;
            }
            else
            {
                context.Docks.Single(dock => dock.Id == dockId).TrailerId = null;
            }
                context.SaveChanges();
        }

        public void ChangeDockCondition(int dockId, string condition,int trailerId)
        {
         var dock = context.Docks.Single(dock => dock.Id == dockId);

            dock.Condition = condition;

            dock.UpdateDateTime = DateTime.Now;

            if (trailerId > 0)
            {
                dock.TrailerId = trailerId;
            }
            else 
            {
                dock.TrailerId = null;
            }
                context.SaveChanges();
        }
    }
}
