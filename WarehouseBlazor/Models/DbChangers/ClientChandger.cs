﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Models.DbChangers
{
    public class ClientChandger : IClientChandger
    {
        private readonly WarehouseBaseContext context;

        public ClientChandger(WarehouseBaseContext context)
        {
            this.context = context;
        }

        public ClientChandger()
        {
            context = new WarehouseBaseContext();
        }

        public void AddClient(Client client)
        {
            context.Clients.Add(client);
            context.SaveChanges();
        }

        public void AddCustomerRepresentative(CustomerRepresentative customer)
        {
            context.СustomerRepresentatives.Add(customer);
            context.SaveChanges();
                
        }
    }
}
