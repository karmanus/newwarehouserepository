﻿using System;
using System.Collections.Generic;
using WarehouseBlazor.Interfaces;

#nullable disable

namespace WarehouseBlazor.Models.MainContext
{
    public partial class StoregeService : IBackEndRepositoryTable
    {
        public StoregeService()
        {
            StoregeCargos = new HashSet<StoregeCargo>();
        }
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public DateTime DateOfStorage { get; set; }
        public DateTime UpdateDateTime { get; set; }
        public int Id { get; set; }

        public virtual Order IdNavigation { get; set; }
        public virtual ICollection<StoregeCargo> StoregeCargos { get; set; }
    }
}
