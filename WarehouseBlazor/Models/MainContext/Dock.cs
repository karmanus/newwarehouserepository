﻿using System;
using System.Collections.Generic;
using WarehouseBlazor.Interfaces;

#nullable disable

namespace WarehouseBlazor.Models.MainContext
{
    public partial class Dock : IBackEndRepositoryTable
    {
        public int Id { get; set; }
        public int DockNumber { get; set; }
        public string Condition { get; set; }
        public int WarehouseId { get; set; }
        public double? DockLocationCoordinateX { get; set; }
        public double? DockLocationCoordinateY { get; set; }
        public double? DockLenght { get; set; }
        public int? TrailerId { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public virtual DockConditionsEnum ConditionNavigation { get; set; }
        public virtual Trailer Trailer { get; set; }
        public virtual Warehouse Warehouse { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
