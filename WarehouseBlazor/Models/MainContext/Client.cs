﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WarehouseBlazor.Interfaces;

#nullable disable

namespace WarehouseBlazor.Models.MainContext
{
    public partial class Client : IBackEndRepositoryTable
    {
        public Client()
        {
            Orders = new HashSet<Order>();

            CustomerRepresentatives = new HashSet<CustomerRepresentative>();
        }

        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime UpdateDateTime { get; set; }
        public string Logo { get; set; } 
        public string PhysicalAddress { get; set; }




        public string TaxId { get; set; }

        public virtual ICollection<Order> Orders { get; set; }


        public virtual ICollection<CustomerRepresentative> CustomerRepresentatives { get; set; }


        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
