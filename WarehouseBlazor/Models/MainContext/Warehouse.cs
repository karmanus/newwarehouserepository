﻿using System;
using System.Collections.Generic;
using WarehouseBlazor.Interfaces;

#nullable disable

namespace WarehouseBlazor.Models.MainContext
{
    public partial class Warehouse : IBackEndRepositoryTable
    {
        public Warehouse()
        {
            Docks = new HashSet<Dock>();
            Sections = new HashSet<Section>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public double? WarehouseTerritoryLenght { get; set; }
        public double? WarehouseTerritoryWidht { get; set; }
        public int? UpperLeftWarhouseCornerX { get; set; }
        public int? UpperLeftWarhouseCornerY { get; set; }
        public int? LowerRightWarehouseCornerX { get; set; }
        public int? LowerRightWarehouseCornerY { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public virtual ICollection<Dock> Docks { get; set; }
        public virtual ICollection<Section> Sections { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
