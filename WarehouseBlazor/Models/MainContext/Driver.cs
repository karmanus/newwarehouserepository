﻿using System;
using System.Collections.Generic;
using WarehouseBlazor.Interfaces;

#nullable disable

namespace WarehouseBlazor.Models.MainContext
{
    public partial class Driver : IBackEndRepositoryTable
    {
        public Driver()
        {
            DeliveryServices = new HashSet<DeliveryService>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public virtual ICollection<DeliveryService> DeliveryServices { get; set; }


        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
