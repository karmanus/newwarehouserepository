﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;

namespace WarehouseBlazor.Models.MainContext
{
    public class CustomerRepresentative : IBackEndRepositoryTable
    {
        public int Id { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public int ClientId { get; set; }
        public string RepName { get; set; }

        public DateTime UpdateDateTime { get; set; }

        public virtual Client Client { get; set; }

        public object Clone()
        {
         return this.MemberwiseClone();
        }

       
    }
}
