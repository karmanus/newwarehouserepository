﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WarehouseBlazor.Models.MainContext
{
    public partial class CargosToBeDelivered : ICloneable
    {
        public int DeliveryServiceId { get; set; }
        public int CargoId { get; set; }
        public string Destination { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public virtual Cargo Cargo { get; set; }
        public virtual DeliveryService DeliveryService { get; set; }


        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
