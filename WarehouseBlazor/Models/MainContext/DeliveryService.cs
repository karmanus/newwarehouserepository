﻿using System;
using System.Collections.Generic;
using WarehouseBlazor.Interfaces;

#nullable disable

namespace WarehouseBlazor.Models.MainContext
{
    public partial class DeliveryService 
    {
        public DeliveryService()
        {
            CargosToBeDelivereds = new HashSet<CargosToBeDelivered>();
        }

        public int? DriverId { get; set; }
        public string ReciperAdress { get; set; }
        public int Id { get; set; }



        public virtual Driver Driver { get; set; }
        public virtual Order IdNavigation { get; set; }
        public virtual ICollection<CargosToBeDelivered> CargosToBeDelivereds { get; set; }


        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
