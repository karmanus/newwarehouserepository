﻿using System;
using System.Collections.Generic;
using WarehouseBlazor.Interfaces;

#nullable disable

namespace WarehouseBlazor.Models.MainContext
{
    public partial class Trailer : IBackEndRepositoryTable
    {
        public Trailer()
        {
            Docks = new HashSet<Dock>();
            PalletsInTrailers = new HashSet<PalletsInTrailer>();
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }


        public int Id { get; set; }
        public string UniqNumnber { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public virtual ICollection<Dock> Docks { get; set; }
        public virtual ICollection<PalletsInTrailer> PalletsInTrailers { get; set; }



    }
}
