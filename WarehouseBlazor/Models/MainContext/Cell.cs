﻿using System;
using System.Collections.Generic;
using WarehouseBlazor.Interfaces;

#nullable disable

namespace WarehouseBlazor.Models.MainContext
{
    public partial class Cell : IBackEndRepositoryTable
    {
        public int Id { get; set; }
        public int Row { get; set; }
        public int CellsColumn { get; set; }
        public int Stack { get; set; }
        public int SectionId { get; set; }
        public int? PalletId { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public virtual Pallet Pallet { get; set; }
        public virtual Section Section { get; set; }


        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
