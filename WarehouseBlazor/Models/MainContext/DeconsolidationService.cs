﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WarehouseBlazor.Models.MainContext
{
    public partial class DeconsolidationService : ICloneable
    {
        public DeconsolidationService()
        {
            DeconsolidatedCargos = new HashSet<DeconsolidatedCargo>();
        }

        public DateTime UpdateDateTime { get; set; }
        public int Id { get; set; }

        public virtual Order IdNavigation { get; set; }
        public virtual ICollection<DeconsolidatedCargo> DeconsolidatedCargos { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
