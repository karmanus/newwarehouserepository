﻿using System;
using System.Collections.Generic;
using WarehouseBlazor.Interfaces;

#nullable disable

namespace WarehouseBlazor.Models.MainContext
{
    public partial class Order : IBackEndRepositoryTable
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public DateTime CreateDateTime { get; set; }
        public virtual Client Client { get; set; }
        public virtual DeconsolidationService DeconsolidationService { get; set; }
        public virtual DeliveryService DeliveryService { get; set; }
        public virtual StoregeService StoregeService { get; set; }


        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
