﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace WarehouseBlazor.Models.MainContext
{
    public partial class WarehouseBaseContext : DbContext
    {
        public WarehouseBaseContext()
        {
        }

        public WarehouseBaseContext(DbContextOptions<WarehouseBaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cargo> Cargos { get; set; }
        public virtual DbSet<CargosConditionsEnum> CargosConditionsEnums { get; set; }
        public virtual DbSet<CargosToBeDelivered> CargosToBeDelivereds { get; set; }
        public virtual DbSet<Cell> Cells { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<DeconsolidatedCargo> DeconsolidatedCargos { get; set; }
        public virtual DbSet<DeconsolidationService> DeconsolidationServices { get; set; }
        public virtual DbSet<DeliveryService> DeliveryServices { get; set; }
        public virtual DbSet<Dock> Docks { get; set; }
        public virtual DbSet<DockConditionsEnum> DockConditionsEnums { get; set; }
        public virtual DbSet<Driver> Drivers { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<Pallet> Pallets { get; set; }
        public virtual DbSet<PalletsInTrailer> PalletsInTrailers { get; set; }
        public virtual DbSet<Section> Sections { get; set; }
        public virtual DbSet<StoregeCargo> StoregeCargos { get; set; }
        public virtual DbSet<StoregeService> StoregeServices { get; set; }
        public virtual DbSet<Trailer> Trailers { get; set; }
        public virtual DbSet<Warehouse> Warehouses { get; set; }

        public virtual DbSet<CustomerRepresentative> СustomerRepresentatives { get; set; }
                             
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                optionsBuilder.UseSqlServer("Data Source=mysuperwarehouseserver.database.windows.net;Initial Catalog=WarehouseBase;User ID=Admiral;Password=Karton11;Connect Timeout=60;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Cyrillic_General_CI_AS");

            modelBuilder.Entity<Cargo>(entity =>
            {
                entity.Property(e => e.Condition)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Discription).HasMaxLength(200);

                entity.Property(e => e.UpdateDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.ConditionNavigation)
                    .WithMany(p => p.Cargos)
                    .HasForeignKey(d => d.Condition)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Cargos_CargosConditionsEnum");
            });

            modelBuilder.Entity<CargosConditionsEnum>(entity =>
            {
                entity.HasKey(e => e.ConditionOfCargo)
                    .HasName("PK_CargosConditions");

                entity.ToTable("CargosConditionsEnum");

                entity.Property(e => e.ConditionOfCargo)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CargosToBeDelivered>(entity =>
            {
                entity.HasKey(e => new { e.DeliveryServiceId, e.CargoId });

                entity.ToTable("CargosToBeDelivered");

                entity.HasIndex(e => e.CargoId, "IX_CargosToBeDelivered")
                    .IsUnique();

                entity.Property(e => e.Destination)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Cargo)
                    .WithOne(p => p.CargosToBeDelivered)
                    .HasForeignKey<CargosToBeDelivered>(d => d.CargoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CargosToBeDelivered_Cargos");

                entity.HasOne(d => d.DeliveryService)
                    .WithMany(p => p.CargosToBeDelivereds)
                    .HasForeignKey(d => d.DeliveryServiceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CargosToBeDelivered_DeliveryServices");
            });

            modelBuilder.Entity<Cell>(entity =>
            {
                entity.HasIndex(e => new { e.Row, e.CellsColumn, e.Stack, e.SectionId }, "IX_Cells")
                    .IsUnique();

                entity.Property(e => e.UpdateDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Pallet)
                    .WithMany(p => p.Cells)
                    .HasForeignKey(d => d.PalletId)
                    .HasConstraintName("FK_Cells_Pallets");

                entity.HasOne(d => d.Section)
                    .WithMany(p => p.Cells)
                    .HasForeignKey(d => d.SectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Cells_Sections");
            });

            modelBuilder.Entity<Client>(entity =>
            {
                entity.ToTable("Client");

                entity.Property(e => e.Logo).HasMaxLength(300);

                entity.Property(e => e.PhysicalAddress).HasMaxLength(200);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TaxId).HasMaxLength(50);

                entity.Property(e => e.UpdateDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<DeconsolidatedCargo>(entity =>
            {
                entity.HasKey(e => new { e.ServiseId, e.CargoId });

                entity.HasIndex(e => e.CargoId, "IX_DeconsolidatedCargos_1")
                    .IsUnique();

                entity.Property(e => e.Destination)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Cargo)
                    .WithOne(p => p.DeconsolidatedCargo)
                    .HasForeignKey<DeconsolidatedCargo>(d => d.CargoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DeconsolidatedCargos_Cargos");

                entity.HasOne(d => d.Servise)
                    .WithMany(p => p.DeconsolidatedCargos)
                    .HasForeignKey(d => d.ServiseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DeconsolidatedCargos_DeconsolidationServices");
            });

            modelBuilder.Entity<DeconsolidationService>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.UpdateDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.DeconsolidationService)
                    .HasForeignKey<DeconsolidationService>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DeconsolidationServices_Orders");
            });

            modelBuilder.Entity<DeliveryService>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ReciperAdress).HasMaxLength(100);

                entity.HasOne(d => d.Driver)
                    .WithMany(p => p.DeliveryServices)
                    .HasForeignKey(d => d.DriverId)
                    .HasConstraintName("FK_DeliveryServices_Drivers1");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.DeliveryService)
                    .HasForeignKey<DeliveryService>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DeliveryServices_Orders");
            });

            modelBuilder.Entity<Dock>(entity =>
            {
                entity.Property(e => e.Condition)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.ConditionNavigation)
                    .WithMany(p => p.Docks)
                    .HasForeignKey(d => d.Condition)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Docks_DockConditionsEnum1");

                entity.HasOne(d => d.Trailer)
                    .WithMany(p => p.Docks)
                    .HasForeignKey(d => d.TrailerId)
                    .HasConstraintName("FK_Docks_Docks1");

                entity.HasOne(d => d.Warehouse)
                    .WithMany(p => p.Docks)
                    .HasForeignKey(d => d.WarehouseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Docks_Warehouses");
            });

            modelBuilder.Entity<DockConditionsEnum>(entity =>
            {
                entity.HasKey(e => e.DockCondition)
                    .HasName("PK_DockConditions");

                entity.ToTable("DockConditionsEnum");

                entity.Property(e => e.DockCondition)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Driver>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.UpdateDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CreateDateTime).HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Orders_Client");
            });

            modelBuilder.Entity<Pallet>(entity =>
            {
                entity.Property(e => e.UpdateDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Cargo)
                    .WithMany(p => p.Pallets)
                    .HasForeignKey(d => d.CargoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Pallets_Cargos1");
            });

            modelBuilder.Entity<PalletsInTrailer>(entity =>
            {
                entity.HasKey(e => new { e.TrailerId, e.PalletId });

                entity.ToTable("PalletsInTrailer");

                entity.HasIndex(e => e.PalletId, "IX_PalletsInTrailer")
                    .IsUnique();

                entity.Property(e => e.UpdateDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Pallet)
                    .WithOne(p => p.PalletsInTrailer)
                    .HasForeignKey<PalletsInTrailer>(d => d.PalletId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PalletsInTrailer_Pallets");

                entity.HasOne(d => d.Trailer)
                    .WithMany(p => p.PalletsInTrailers)
                    .HasForeignKey(d => d.TrailerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PalletsInTrailer_Trailers");
            });

            modelBuilder.Entity<Section>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Warehouse)
                    .WithMany(p => p.Sections)
                    .HasForeignKey(d => d.WarehouseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Sections_Warehouses");
            });

            modelBuilder.Entity<StoregeCargo>(entity =>
            {
                entity.HasKey(e => new { e.StrogeServiceId, e.CargoId });

                entity.HasIndex(e => e.CargoId, "IX_StoregeCargos")
                    .IsUnique();

                entity.Property(e => e.UpdateDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Cargo)
                    .WithOne(p => p.StoregeCargo)
                    .HasForeignKey<StoregeCargo>(d => d.CargoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StoregeCargos_Cargos");

                entity.HasOne(d => d.StrogeService)
                    .WithMany(p => p.StoregeCargos)
                    .HasForeignKey(d => d.StrogeServiceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StoregeCargos_StoregeServices");
            });

            modelBuilder.Entity<StoregeService>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.DateOfStorage).HasColumnType("date");

                entity.Property(e => e.UpdateDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.StoregeService)
                    .HasForeignKey<StoregeService>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StoregeServices_Orders");
            });

            modelBuilder.Entity<Trailer>(entity =>
            {
                entity.Property(e => e.UniqNumnber)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Warehouse>(entity =>
            {
                entity.Property(e => e.Address).HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UpdateDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<CustomerRepresentative>(entity =>
            {
                entity.Property(e => e.Phone).HasMaxLength(100);

                entity.Property(e => e.Email).HasMaxLength(100);

                entity.Property(e => e.ClientId).IsRequired();

                entity.Property(e => e.RepName).HasMaxLength(50);


                entity.Property(e => e.UpdateDateTime)
                   .HasColumnType("datetime")
                   .HasDefaultValueSql("(getdate())");


                entity.HasOne(d => d.Client)
                   .WithMany(p => p.CustomerRepresentatives)
                   .HasForeignKey(d => d.ClientId)
                   .OnDelete(DeleteBehavior.ClientSetNull)
                   .HasConstraintName("FK_RepCust_Client");

            });
            
                                

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
