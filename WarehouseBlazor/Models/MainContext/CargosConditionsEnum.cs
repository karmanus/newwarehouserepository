﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WarehouseBlazor.Models.MainContext
{
    public partial class CargosConditionsEnum
    {
        public CargosConditionsEnum()
        {
            Cargos = new HashSet<Cargo>();
        }

        public string ConditionOfCargo { get; set; }

        public virtual ICollection<Cargo> Cargos { get; set; }
    }
}
