﻿using System;
using System.Collections.Generic;
using WarehouseBlazor.Interfaces;

#nullable disable

namespace WarehouseBlazor.Models.MainContext
{
    public partial class Cargo : IBackEndRepositoryTable
    {
        public Cargo()
        {
            Pallets = new HashSet<Pallet>();
        }

        public int Id { get; set; }
        public string Discription { get; set; }
        public string Condition { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public virtual CargosConditionsEnum ConditionNavigation { get; set; }
        public virtual CargosToBeDelivered CargosToBeDelivered { get; set; }
        public virtual DeconsolidatedCargo DeconsolidatedCargo { get; set; }
        public virtual StoregeCargo StoregeCargo { get; set; }
        public virtual ICollection<Pallet> Pallets { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
