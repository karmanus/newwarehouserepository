﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WarehouseBlazor.Models.MainContext
{
    public partial class PalletsInTrailer : ICloneable
    {
        public int TrailerId { get; set; }
        public int PalletId { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public virtual Pallet Pallet { get; set; }
        public virtual Trailer Trailer { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
