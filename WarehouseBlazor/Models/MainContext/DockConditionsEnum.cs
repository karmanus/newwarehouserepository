﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WarehouseBlazor.Models.MainContext
{
    public partial class DockConditionsEnum
    {
        public DockConditionsEnum()
        {
            Docks = new HashSet<Dock>();
        }

        public string DockCondition { get; set; }

        public virtual ICollection<Dock> Docks { get; set; }
    }
}
