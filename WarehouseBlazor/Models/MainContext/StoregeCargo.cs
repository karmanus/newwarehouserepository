﻿using System;
using System.Collections.Generic;
using WarehouseBlazor.Interfaces;

#nullable disable

namespace WarehouseBlazor.Models.MainContext
{
    public partial class StoregeCargo : ICloneable
    {
        public int StrogeServiceId { get; set; }
        public int CargoId { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public virtual Cargo Cargo { get; set; }
        public virtual StoregeService StrogeService { get; set; }


        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
