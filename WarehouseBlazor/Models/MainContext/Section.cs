﻿using System;
using System.Collections.Generic;
using WarehouseBlazor.Interfaces;

#nullable disable

namespace WarehouseBlazor.Models.MainContext
{
    public partial class Section : IBackEndRepositoryTable
    {
        public Section()
        {
            Cells = new HashSet<Cell>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int WarehouseId { get; set; }
        public int? LeftUpperSectionCornerX { get; set; }
        public int? LeftUpperSectionCornerY { get; set; }
        public int? LeftLowerSectionCornerX { get; set; }
        public int? LeftLowerSectionCornerY { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public virtual Warehouse Warehouse { get; set; }
        public virtual ICollection<Cell> Cells { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
