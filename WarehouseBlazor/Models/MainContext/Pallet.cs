﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WarehouseBlazor.Models.MainContext
{
    public partial class Pallet : ICloneable
    {
        public Pallet()
        {
            Cells = new HashSet<Cell>();           
        }

        

        public int Id { get; set; }
        public int CargoId { get; set; }
        public double? Length { get; set; }
        public double? Width { get; set; }
        public double? Height { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public virtual Cargo Cargo { get; set; }
        public virtual PalletsInTrailer PalletsInTrailer { get; set; }
        public virtual ICollection<Cell> Cells { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
