﻿using BlazorDateRangePicker;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Models.DbGetters
{
    public class OrdersGetter : IOrdersGetter
    {
        private readonly WarehouseBaseContext context;

        public OrdersGetter()
        {
            context = new WarehouseBaseContext();
        }
        
        
        public List<Order> GetAllOrders()
        {
            return context.Orders.ToList();
        }

        public DeliveryService GetDeliveryService(int id)
        {
            return context.DeliveryServices.Include("Cargos").Single(deliveryService => deliveryService.Id == id);
        }

        public DeconsolidationService GetDeonsolidateServices(int id)
        {
            return context.DeconsolidationServices.Include("Cargos").Single(deconsolidationService => deconsolidationService.Id == id);
        }

        public Order GetOrderById(int id)
        {
           return context.Orders.SingleOrDefault(order => order.Id == id);
        }

        public List<Order> GetOrdersByDateRange(DateRange dateRange)
        {
            return context.Orders.Where(order => order.CreateDateTime >= dateRange.Start && order.CreateDateTime <= dateRange.End).Include("Client").Include("DeconsolidationService").Include("DeliveryService").
              Include("StoregeService").ToList();
        }

        public StoregeService GetStoregeService(int id)
        {
            return context.StoregeServices.Include("Cargos").Single(storageService=> storageService.Id == id);
        }
    }
}
