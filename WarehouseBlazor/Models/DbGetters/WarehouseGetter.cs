﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Models.DbGetters
{
    public class WarehouseGetter : IWarehouseGetter
    {

        private readonly WarehouseBaseContext context;

        public WarehouseGetter()
        {
            context = new WarehouseBaseContext();
        }

        public WarehouseGetter(WarehouseBaseContext context)
        {
            this.context = context;
        }

        public Warehouse GetWarehouseById(int warehouseId)
        {
            return context.Warehouses.Single(warehouse => warehouse.Id == warehouseId);
        }

        public List<Warehouse> GetWarehouses()
        {
            return context.Warehouses.ToList();
        }
    }
}
