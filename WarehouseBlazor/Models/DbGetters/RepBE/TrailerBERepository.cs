﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Models.DBRep;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Models.DbGetters.RepBE
{
    public class TrailerBERepository : ITrailerGetter
    {
        private readonly DbBackEndRepository repository;

        public TrailerBERepository(DbBackEndRepository repository)
        {
            this.repository = repository;
        }

        public List<Trailer> GetAllTrailers()
        {
           return repository.Trailers.GetEntries().ToList();
        }

        public List<Trailer> GetListOfTrailersNotInTheDock()
        {
            return repository.Trailers.GetEntries().Where(x => !repository.Docks.GetEntries().Select(x => x.TrailerId).Contains(x.Id)).ToList();
        }

        public Trailer GetTrailerById(int trailerId)
        {
            return repository.Trailers.GetEntries().Single(trailer=>trailer.Id == trailerId);
        }
    }
}
