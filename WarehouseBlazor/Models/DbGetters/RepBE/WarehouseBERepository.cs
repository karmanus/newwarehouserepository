﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Models.DBRep;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Models.DbGetters.RepBE
{
    public class WarehouseBERepository : IWarehouseGetter
    {
        DbBackEndRepository repository;

        public WarehouseBERepository(DbBackEndRepository repository)
        {
            this.repository = repository;
        }

        public Warehouse GetWarehouseById(int warehouseId)
        {
            return repository.Warehouses.GetEntries().Single(warehouse=> warehouse.Id == warehouseId);
        }

        public List<Warehouse> GetWarehouses()
        {
          return  repository.Warehouses.GetEntries();
        }
    }
}
