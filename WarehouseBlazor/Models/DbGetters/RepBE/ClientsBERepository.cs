﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Models.DBRep;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Models.DbGetters.RepBE
{
    public class ClientsBERepository : IClientGetter
    {
        DbBackEndRepository repository;

        public ClientsBERepository(DbBackEndRepository repository)
        {
            this.repository = repository;
        }

        public List<Client> GetAllClients()
        {
           return repository.Clients.GetEntries().ToList();
        }

        public Client GetClient(int clientId)
        {
            return repository.Clients.GetEntries().Single(client => client.Id == clientId);
        }
    }
}
