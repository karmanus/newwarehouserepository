﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Models.DBRep;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Models.DbGetters.RepBE
{
    public class DockBERepository : IDockGetter
    {
        DbBackEndRepository repository;

        public DockBERepository(DbBackEndRepository repository)
        {
            this.repository = repository;
        }

        public Dock GetDockById(int dockId)
        {
            return repository.Docks.GetEntries().Single(dock=> dock.Id ==  dockId);
        }

        public List<Dock> GetWarehouseDocks(int warehouseId)
        {
            return repository.Docks.GetEntries().Where(dock=> dock.WarehouseId == warehouseId).ToList();
        }
    }
}
