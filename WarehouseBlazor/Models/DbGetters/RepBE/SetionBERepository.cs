﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Models.DBRep;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Models.DbGetters.RepBE
{
    public class SetionBERepository : ISectionsGetter
    {
        DbBackEndRepository repository;

        public SetionBERepository(DbBackEndRepository repository)
        {
            this.repository = repository;
        }


        public Section GetSectionById(int sectionId)
        {
            return repository.Sections.GetEntries().Single(section => section.Id==sectionId);
        }

        public List<Section> GetSectionsByWarehouseId(int warehouseId)
        {
            return repository.Sections.GetEntries().Where(section => section.WarehouseId == warehouseId).ToList();
        }
    }
}
