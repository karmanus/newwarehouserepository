﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Models.DBRep;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Models.DbGetters.RepBE
{
    public class CellsBERepository : ICellsGetter
    {
        DbBackEndRepository repository;

        public CellsBERepository(DbBackEndRepository repository)
        {
            this.repository = repository;
        }

        public Cell GetCellById(int cellId)
        {
            return repository.Cells.GetEntries().Single(cell=> cell.Id == cellId);
        }

        public List<Cell> GetCellsBySectionId(int sectionId)
        {
            return repository.Cells.GetEntries().Where(cell => cell.SectionId == sectionId).ToList();
        }
    }
}
