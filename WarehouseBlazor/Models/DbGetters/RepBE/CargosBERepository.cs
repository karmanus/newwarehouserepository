﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Models.Conditions;
using WarehouseBlazor.Models.DBRep;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Models.DbGetters.RepBE
{
    public class CargosBERepository : ICargosGetter
    {
        private readonly DbBackEndRepository repository;

        public CargosBERepository(DbBackEndRepository repository)
        {
            this.repository = repository;
        }

        public List<Cargo> GetAllCargos()
        {
           return repository.Cargos.GetEntries().ToList();
        }

        public Cargo GetCargoById(int cargoId)
        {
            return repository.Cargos.GetEntries().Single(cargo => cargo.Id == cargoId);
        }

        public List<Cargo> SelectCargosByCondition(CargoConditions condition)
        {
            throw new NotImplementedException();
        }
    }
}
