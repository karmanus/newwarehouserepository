﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Models.DbGetters
{
    public class TrailerGetter : ITrailerGetter
    {
        WarehouseBaseContext context;

        public TrailerGetter()
        {
            context = new WarehouseBaseContext();
        
        }

        public List<Trailer> GetAllTrailers()
        {
            return context.Trailers.ToList();
        }

        public List<Trailer> GetListOfTrailersNotInTheDock()
        {
            return context.Trailers.Where(x => !context.Docks.Select(x => x.TrailerId).Contains(x.Id)).ToList();

            //var query = from trailer in context.Trailers
            //            join dock in context.Docks on trailer.Id  equals dock.TrailerId
            //            into groupL
            //            from docks in groupL.DefaultIfEmpty()
            //            select new Trailer() { Id = trailer.Id, UniqNumnber = trailer.UniqNumnber };                      

            //var freeTr = context.Trailers.Join(context.Docks,
            //   t => t.Id,
            //   d => d.TrailerId,
            //   (t, d) => new Trailer { Id = t.Id, UniqNumnber = t.UniqNumnber }).ToList();
            //var Ids = context.Docks.Select(x => x.TrailerId).ToList();
        }

        public Trailer GetTrailerById(int trailerId)
        {
            return context.Trailers.Single(trailer=> trailer.Id == trailerId);
        }
    }
}
