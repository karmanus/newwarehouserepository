﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Models.DbGetters
{
    public class DockConditionEnumGetter : IDockConditionEnumGetter
    {

        private readonly WarehouseBaseContext context;

        public DockConditionEnumGetter()
        {
            context = new WarehouseBaseContext();
        }

        public DockConditionEnumGetter(WarehouseBaseContext context)
        {
            this.context = context;
        }

        public List<string> GetAllConditionsEnum()
        {
            return context.DockConditionsEnums.Select(condition => condition.DockCondition).ToList();
        }
    }
}
