﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Models.DbGetters
{
    public class ClientGetter : IClientGetter
    {
        public readonly WarehouseBaseContext context;

        public ClientGetter()
        {
            context = new WarehouseBaseContext();
        }

        public ClientGetter(WarehouseBaseContext context)
        {
            this.context = context;
        }

        public List<Client> GetAllClients()
        {
            return context.Clients.Include(x=>x.CustomerRepresentatives).ToList();
        }

        public Client GetClient(int clientId)
        {
            return context.Clients.Include(x => x.CustomerRepresentatives).Single(client => client.Id == clientId);
        }
    }
}
