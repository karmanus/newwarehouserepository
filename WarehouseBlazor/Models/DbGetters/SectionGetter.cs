﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Models.DbGetters
{
       
    
    public class SectionGetter :ISectionsGetter
    {
        private readonly WarehouseBaseContext context;

        public SectionGetter()
        {
            context = new WarehouseBaseContext();
        }

        public SectionGetter(WarehouseBaseContext context)
        {
            this.context = context;
        }

        public Section GetSectionById(int sectionId)
        {
          return  context.Sections.Single(section => section.Id == sectionId);
        }

        public List<Section> GetSectionsByWarehouseId(int warehouseId)
        {
            return context.Sections.Where(section => section.WarehouseId == warehouseId).ToList();
        }
    }
}
