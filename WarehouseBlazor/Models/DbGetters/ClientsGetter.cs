﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Interfaces;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Models.DbGetters
{
    public class ClientsGetter : IClientsGetter
    {
        WarehouseBaseContext context;

        public ClientsGetter(WarehouseBaseContext context)
        {
            this.context = context;
        }

        public ClientsGetter()
        {
            context = new WarehouseBaseContext();
        }
        public List<Client> GetAllClients()
        {
           return context.Clients.ToList();
        }

        public Client GetClientById(int clientId)
        {
           return context.Clients.Single(client => client.Id == clientId);
        }
    }
}
