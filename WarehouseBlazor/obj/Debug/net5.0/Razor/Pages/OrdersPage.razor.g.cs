#pragma checksum "C:\XProject\newwarehouserepository\WarehouseBlazor\Pages\OrdersPage.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5755fad6feeff7783ab932134be884537699fa34"
// <auto-generated/>
#pragma warning disable 1591
namespace WarehouseBlazor.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\XProject\newwarehouserepository\WarehouseBlazor\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\XProject\newwarehouserepository\WarehouseBlazor\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\XProject\newwarehouserepository\WarehouseBlazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\XProject\newwarehouserepository\WarehouseBlazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\XProject\newwarehouserepository\WarehouseBlazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\XProject\newwarehouserepository\WarehouseBlazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\XProject\newwarehouserepository\WarehouseBlazor\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\XProject\newwarehouserepository\WarehouseBlazor\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\XProject\newwarehouserepository\WarehouseBlazor\_Imports.razor"
using WarehouseBlazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\XProject\newwarehouserepository\WarehouseBlazor\_Imports.razor"
using WarehouseBlazor.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\XProject\newwarehouserepository\WarehouseBlazor\_Imports.razor"
using WarehouseBlazor.Models.DbChacking;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\XProject\newwarehouserepository\WarehouseBlazor\_Imports.razor"
using WarehouseBlazor.Models.MainContext;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\XProject\newwarehouserepository\WarehouseBlazor\_Imports.razor"
using WarehouseBlazor.Interfaces;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\XProject\newwarehouserepository\WarehouseBlazor\_Imports.razor"
using WarehouseBlazor.Models.Conditions;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\XProject\newwarehouserepository\WarehouseBlazor\_Imports.razor"
using WarehouseBlazor.Models.DbGetters;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\XProject\newwarehouserepository\WarehouseBlazor\_Imports.razor"
using WarehouseBlazor.Models.Enums;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "C:\XProject\newwarehouserepository\WarehouseBlazor\_Imports.razor"
using System.IO;

#line default
#line hidden
#nullable disable
#nullable restore
#line 21 "C:\XProject\newwarehouserepository\WarehouseBlazor\_Imports.razor"
using BlazorDateRangePicker;

#line default
#line hidden
#nullable disable
#nullable restore
#line 23 "C:\XProject\newwarehouserepository\WarehouseBlazor\_Imports.razor"
using WarehouseBlazor.Models.DBRep;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/OrdersPage")]
    public partial class OrdersPage : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<h2>Select Data Range</h2>\r\n");
            __builder.OpenElement(1, "div");
            __builder.AddAttribute(2, "class", "row");
            __builder.OpenComponent<BlazorDateRangePicker.DateRangePicker>(3);
            __builder.AddAttribute(4, "style", "margin-top: 40px; margin-left:20px; margin-bottom:30px;");
            __builder.AddAttribute(5, "OnRangeSelect", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<BlazorDateRangePicker.DateRange>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<BlazorDateRangePicker.DateRange>(this, 
#nullable restore
#line 8 "C:\XProject\newwarehouserepository\WarehouseBlazor\Pages\OrdersPage.razor"
                                                                                                    OnRangeSelect

#line default
#line hidden
#nullable disable
            )));
            __builder.AddAttribute(6, "TimePicker", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean?>(
#nullable restore
#line 8 "C:\XProject\newwarehouserepository\WarehouseBlazor\Pages\OrdersPage.razor"
                                                                                                                               true

#line default
#line hidden
#nullable disable
            ));
            __builder.CloseComponent();
            __builder.AddMarkupContent(7, "\r\n    <img src=\"/images/calendar.png\" width=\"30\" height=\"30\" style=\"margin-top: 40px; \">");
            __builder.CloseElement();
#nullable restore
#line 14 "C:\XProject\newwarehouserepository\WarehouseBlazor\Pages\OrdersPage.razor"
 if (orders != null)
{
    

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\XProject\newwarehouserepository\WarehouseBlazor\Pages\OrdersPage.razor"
     foreach (var order in orders)
    {

#line default
#line hidden
#nullable disable
            __builder.OpenComponent<WarehouseBlazor.Shared.OrderView>(8);
            __builder.AddAttribute(9, "Order", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<WarehouseBlazor.Models.MainContext.Order>(
#nullable restore
#line 18 "C:\XProject\newwarehouserepository\WarehouseBlazor\Pages\OrdersPage.razor"
                          order

#line default
#line hidden
#nullable disable
            ));
            __builder.CloseComponent();
#nullable restore
#line 19 "C:\XProject\newwarehouserepository\WarehouseBlazor\Pages\OrdersPage.razor"
    }

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "C:\XProject\newwarehouserepository\WarehouseBlazor\Pages\OrdersPage.razor"
     
}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
#nullable restore
#line 28 "C:\XProject\newwarehouserepository\WarehouseBlazor\Pages\OrdersPage.razor"
       



    List<Order> orders;

    void OnRangeSelect(DateRange range)
    {
        if (range != null)
        {
            orders = ordersGetter.GetOrdersByDateRange(range);
        }
        else
        {
            orders = null;
        }
    }



    protected override void OnInitialized()
    {

    }



#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IOrdersGetter ordersGetter { get; set; }
    }
}
#pragma warning restore 1591
