﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Interfaces
{
    interface ICellsGetter
    {
        public Cell GetCellById(int CellId);

        public List<Cell> GetCellsBySectionId(int SectionId);
    }
}
