﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Interfaces
{
    interface IWarehouseGetter
    {
        public Warehouse GetWarehouseById(int warehouseId);

        public List<Warehouse> GetWarehouses();
    }
}
