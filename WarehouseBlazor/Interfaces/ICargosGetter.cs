﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Models.Conditions;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Interfaces
{
    interface ICargosGetter
    {
        public List<Cargo> GetAllCargos();

        public Cargo GetCargoById(int cargoId);

        public List<Cargo> SelectCargosByCondition(CargoConditions condition);

    }
}
