﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseBlazor.Interfaces
{
    interface ICellsManipulator
    {
        public void AddPalletInCell(int palletId, int cellId);

        public void AddPalletInTrailer(int palletId, int trailerId);
    }
}
