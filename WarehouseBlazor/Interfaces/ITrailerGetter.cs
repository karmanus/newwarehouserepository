﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Interfaces
{
    public interface ITrailerGetter
    {
        public Trailer GetTrailerById(int trailerId);

        public List<Trailer> GetAllTrailers();

        public List<Trailer> GetListOfTrailersNotInTheDock();


    }
}
