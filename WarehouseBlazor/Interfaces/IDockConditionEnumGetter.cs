﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseBlazor.Interfaces
{
    public interface IDockConditionEnumGetter
    {
        public List<string> GetAllConditionsEnum();
    }
}
