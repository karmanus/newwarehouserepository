﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Interfaces
{
    public interface IClientGetter
    {
        public List<Client> GetAllClients();

        public Client GetClient(int clientId);
    }
}
