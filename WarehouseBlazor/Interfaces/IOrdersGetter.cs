﻿using BlazorDateRangePicker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Interfaces
{
    public interface IOrdersGetter
    {
        public List<Order> GetAllOrders();

        public List<Order> GetOrdersByDateRange(DateRange data);

        public Order GetOrderById(int id);

        public DeconsolidationService GetDeonsolidateServices(int id);

        public DeliveryService GetDeliveryService(int id);

        public StoregeService GetStoregeService(int id);


    }
}
