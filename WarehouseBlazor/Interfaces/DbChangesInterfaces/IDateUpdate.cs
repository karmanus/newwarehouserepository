﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseBlazor.Interfaces.DbChangesInterfaces
{
    public interface IDateUpdate
    {
        public DateTime UpdateDateTime { get; set; }
    }
}
