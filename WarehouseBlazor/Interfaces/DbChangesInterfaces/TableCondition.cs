﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseBlazor.Interfaces.DbChangesInterfaces
{
    public class TableCondition
    {
        public DateTime LastTableUpdate { get; set; }

        public int LastTableCount { get; set; }
    }
}
