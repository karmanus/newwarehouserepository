﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseBlazor.Interfaces
{
   public interface IClone<T>
    {
        
       public IClone<T> Clone();

    
    }
}
