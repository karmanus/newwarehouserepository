﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Interfaces
{
    interface IDockGetter
    {
        public List<Dock> GetWarehouseDocks(int warehouseId);

        public Dock GetDockById(int dockId);

    }
}
