﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseBlazor.Interfaces
{
    interface IDockChanger
    {
        public void ChangeDockCondition(int dockId, string condition,int trailerId);


        public void AddTrailerOnDock(int dockId, int trailerId);
    }
}
