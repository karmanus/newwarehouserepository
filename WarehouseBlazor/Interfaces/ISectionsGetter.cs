﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Interfaces
{
    interface ISectionsGetter
    {
        public Section GetSectionById(int sectionId);

        public List<Section> GetSectionsByWarehouseId(int warehouseId);

    }
}
