﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Interfaces
{
    interface IClientsGetter
    {

        public Client GetClientById(int id);

        public List<Client> GetAllClients();
            
    }
}
