﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseBlazor.Interfaces
{
    public interface IDbTableChangeChecker
    {           
        public string TableName { get;}
        public void CheckTable();

      

        public  event Action TableChanged;
    }
}
