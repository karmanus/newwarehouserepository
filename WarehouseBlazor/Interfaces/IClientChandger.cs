﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseBlazor.Models.MainContext;

namespace WarehouseBlazor.Interfaces
{
    public interface IClientChandger
    {

        public void AddClient(Client client);

        public void AddCustomerRepresentative(CustomerRepresentative customer);
    
    }
}
