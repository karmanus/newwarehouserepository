﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarehouseBlazor.Interfaces
{
    public interface IDbChangesChecker
    {
        public void CheckTable(string tableName);

        public void CheckAllTables();

        public void SubscribeToChangeEventByTableName(string tableName, Action action);
    }
}
